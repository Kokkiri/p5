const canvas = document.querySelector('canvas')
const context = canvas.getContext('2d')
const width = canvas.width = window.innerWidth
const height = canvas.height = window.innerHeight
let noons = []
let noon_total = 200
let color_num = 25

const Noon = function () {
    this.x = Math.random() * canvas.width
    this.y = Math.random() * canvas.height
    this.speed_y = this.radius = this.zindex = Math.round(Math.random() * 10 + 1)
    // this.speed_x = 0

    this.draw = function() {
        context.beginPath()
        context.arc(this.x, this.y, this.radius, Math.PI * 2, false)
        context.fillStyle = `rgb(${color_num * this.radius}, ${color_num * this.radius}, ${color_num * this.radius})`
        context.fill()
        context.closePath()
        
        // UPDATE
        // this.x += this.speed_x
        this.y += this.speed_y / 2
        if (this.y > canvas.height) {
            this.y = -20
        }
    }
}

function list_noon() {
    for (let i = 0; i < noon_total; i++) {
        noons.push(new Noon)
    }
}
list_noon()

const first_map = noons.map((el, index) => {
    return {index: index, value: el.zindex}
})
first_map.sort(function (a, b) {
    return a.value - b.value;
});
const second_map = first_map.map(function (el) {
    return noons[el.index]
});


function animate() {
    context.fillStyle = 'black'
    context.fillRect(0, 0, width, height)
    // create_noon()
    second_map.forEach(noon => {
        noon.draw()
    });
    requestAnimationFrame(animate)
}

(() => {
    // list_noon()
    animate()
})()
