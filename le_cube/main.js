// camera
let angle = 0 // rotation angle of cubes
let rot_speed = 8 // le multiple par lequel on multiplie angle
let cam_rot_speed = 4 // rotation speed of camera
let cam_dist = 400 // rayon entre point d'origin et camera
let zoom = false
let cam_angle_x = 90
let cam_angle_y = 0
let cam_up_y = 1
// animation
let animation // cube's transition animation
let animation_list = [0, 1, 2, 3, 4, 5]
let anim // index of animation_list
let configuration = 0
let manip = true // do not manipulate cube while animation
let cs = 30 // cube_size
let hcs = cs / 2 // half_cube_size
let pos_y = 0 // global position of the entire objet in the space
let cubes = []
let reverse = false
// environment
let planets = []
// color
let yellow
let green
let blue
let dark_purple
let dark_purple_2
let dark_red
let dark_blue
let dark_orange
let dark_green
let red // color piece
let bg // background and ambiant font
// path
let shape_id = 1
let last_shape = "2a"
let current_shape = "1"
let new_shape
// let path
let path_enable
let model_name // current_path of the current_shape
let shapes
// piece
let piece
let piece_anim = false
let piece_anim_speed = 2
let piece_pos_x = 0
let piece_pos_y = 0
let is_input = false // does the piece enter or exit the shape
let come_from // need to know where does the piece come from when it enter into a new shape
let start_angle
let agl_rot_piece
let agl_rot_piece_speed = 6
// start
let launching = true // is it the first time we launch the program

// function preload() {
//     let data = 'graph.json'
//     path = loadJSON(data)
// }

function setup() {
    createCanvas(600, 600, WEBGL)
    angleMode(DEGREES)

    yellow = color(255, 200, 20)
    yellow_2 = color(245, 230, 0)
    orange = color(255, 150, 70)
    green = color(120, 255, 100)
    green_2 = color(180, 215, 80)
    blue = color(100, 120, 255)
    dark_purple = color(150 ,100 ,150)
    dark_purple_2 = color(170 ,110 ,120)
    dark_orange = color(180 ,140 ,80)
    dark_blue = color(100 ,100 ,200)
    dark_red = color(200, 100, 100)
    dark_green = color(100 ,200 ,100)
    bg = color(255, 255, 150)
    red = color(255, 20, 20)

    piece = new Piece()
    // setAttributes('antialias', true)
    for (i = -.5; i < 1.5; i++){
        for (j = -.5; j < 1.5; j++) {
            for (k = -.5; k < 1.5; k++) {
                cubes.push(new Cube(0, 0, 0))
            }
        }
    }
    for (i = 0; i < 20; i ++) {
        planets.push(new Planet())
    }
    // cube 0
    shape_1 = new Shape(1, model_1, 90, 0, -90, yellow)
    shape_2 = new Shape(2, model_2, 0, 90, 0, dark_orange)
    shape_3 = new Shape(3, model_8, 90, 0, 0, dark_red)
    shape_4 = new Shape(4, model_4, 0, -90, 90, blue)
    shape_5 = new Shape(5, model_4, 0, 180, 0, green)
    shape_6 = new Shape(6, model_4, 0, 0, 0, dark_purple)
    // cube 3
    shape_7 = new Shape(7, model_4, 90, 0, 0, yellow)
    shape_8 = new Shape(8, model_1, 0, 90, -90, orange)
    shape_9 = new Shape(9, model_2, -90, 0, 90, dark_red)
    shape_10 = new Shape(10, model_2, 0, -90, 0, dark_orange)
    shape_11 = new Shape(11, model_4, 0, 180, 180, green)
    shape_12 = new Shape(12, model_5, 0, 0, 180, dark_purple_2)
    // cube 7
    shape_13 = new Shape(13, model_1, 90, 0, -90, yellow)
    shape_14 = new Shape(14, model_5, 0, 90, 180, dark_orange)
    shape_15 = new Shape(15, model_2, 90, 180, 0, dark_blue)
    shape_16 = new Shape(16, model_3, 0, 90, 90, blue)
    shape_17 = new Shape(17, model_4, 0, 180, 0, dark_purple)
    shape_18 = new Shape(18, model_4, 0, 0, 0, green_2)
    // cube 4
    shape_19 = new Shape(19, model_4, 90, 0, 180, yellow)
    shape_20 = new Shape(20, model_4, 0, 90, 90, orange)
    shape_21 = new Shape(21, model_4, -90, 0, -90, dark_blue)
    shape_22 = new Shape(22, model_5, 0, -90, 90, dark_orange)
    shape_23 = new Shape(23, model_3, 0, 0, 0, dark_purple_2)
    shape_24 = new Shape(24, model_4, 0, 0, 180, green_2)
    // cube 1
    shape_25 = new Shape(25, model_3, 90, 0, 0, dark_red)
    shape_26 = new Shape(26, model_4, 0, 90, 0, dark_green)
    shape_27 = new Shape(27, model_4, -90, 0, -90, yellow_2)
    shape_28 = new Shape(28, model_4, 0, -90, 180, blue)
    shape_29 = new Shape(29, model_5, 0, 180, 90, green)
    shape_30 = new Shape(30, model_3, 0, 0, 0, dark_purple)
    // cube 2
    shape_31 = new Shape(31, model_4, 90, 0, -90, dark_red)
    shape_32 = new Shape(32, model_3, 0, 90, 90, orange)
    shape_33 = new Shape(33, model_4, -90, 0, -90, yellow_2)
    shape_34 = new Shape(34, model_4, 0, -90, 90, dark_green)
    shape_35 = new Shape(35, model_4, 0, 180, -90, green)
    shape_36 = new Shape(36, model_4, 0, 0, 0, dark_purple_2)
    // cube 6
    shape_37 = new Shape(37, model_3, 90, 0, 90, dark_blue)
    shape_38 = new Shape(38, model_5, 180, 90, 0, dark_green)
    shape_39 = new Shape(39, model_7, 90, 180, 0, yellow_2)
    shape_40 = new Shape(40, model_4, 0, -90, 0, blue)
    shape_41 = new Shape(41, model_6, 0, 180, 90, dark_purple)
    shape_42 = new Shape(42, model_3, 0, 0, 0, green_2)
    // cube 5
    shape_43 = new Shape(43, model_4, 90, 0, -90, dark_blue)
    shape_44 = new Shape(44, model_5, 0, 90, -90, orange)
    shape_45 = new Shape(45, model_4, -90, 0, -90, yellow_2)
    shape_46 = new Shape(46, model_5, 0, -90, 180, dark_green)
    shape_47 = new Shape(47, model_6, 0, 180, -90, dark_purple_2)
    shape_48 = new Shape(48, model_1, 0, 0, 0, green_2)
    
    cubes[0].set_trans(-cs, 0, -hcs)
    cubes[0].set_pos(hcs, -hcs, 0)
    cubes[0].set_shape(shape_2, shape_6, shape_4, shape_5, shape_1, shape_3)
    
    cubes[1].set_trans(-hcs, hcs, 0)
    cubes[1].set_pos(hcs, hcs, 0)
    cubes[1].set_shape(shape_26, shape_30, shape_28, shape_29, shape_25, shape_27)
    
    cubes[2].set_trans(hcs, 0, -hcs)
    cubes[2].set_pos(hcs, 0, hcs)
    cubes[2].set_shape(shape_32, shape_36, shape_34, shape_35, shape_31, shape_33)
    
    cubes[3].set_trans(hcs, -hcs, 0)
    cubes[3].set_pos(-hcs, -hcs, 0)
    cubes[3].set_shape(shape_8, shape_12, shape_10, shape_11, shape_7, shape_9)
    
    cubes[4].set_trans(0, -hcs, hcs)
    cubes[4].set_pos(0, hcs, hcs)
    cubes[4].set_shape(shape_20, shape_24, shape_22, shape_23, shape_19, shape_21)
    
    cubes[5].set_trans(hcs, hcs, 0)
    cubes[5].set_pos(-hcs, hcs, 0)
    cubes[5].set_shape(shape_44, shape_48, shape_46, shape_47, shape_43, shape_45)
    
    cubes[6].set_trans(-hcs, 0, hcs)
    cubes[6].set_pos(-hcs, 0, -hcs)
    cubes[6].set_shape(shape_38, shape_42, shape_40, shape_41, shape_37, shape_39)
    
    cubes[7].set_trans(-hcs, -hcs, 0)
    cubes[7].set_pos(hcs, -hcs, 0)
    cubes[7].set_shape(shape_14, shape_18, shape_16, shape_17, shape_13, shape_15)

    shapes = {
        "1": shape_1,
        "2a": shape_2,
        "2b": shape_2,
        "3a": shape_3,
        "3b": shape_3,
        "4": shape_4,
        "5": shape_5,
        "6": shape_6,
        "7": shape_7,
        "8": shape_8,
        "9a": shape_9,
        "9b": shape_9,
        "10a": shape_10,
        "10b": shape_10,
        "11": shape_11,
        "12a": shape_12,
        "12b": shape_12,
        "13": shape_13,
        "14a": shape_14,
        "14b": shape_14,
        "15a": shape_15,
        "15b": shape_15,
        "16": shape_16,
        "17": shape_17,
        "18": shape_18,
        "19": shape_19,
        "20": shape_20,
        "21": shape_21,
        "22a": shape_22,
        "22b": shape_22,
        "23": shape_23,
        "24": shape_24,
        "25": shape_25,
        "26": shape_26,
        "27": shape_27,
        "28": shape_28,
        "29a": shape_29,
        "29b": shape_29,
        "30": shape_30,
        "31": shape_31,
        "32": shape_32,
        "33": shape_33,
        "34": shape_34,
        "35": shape_35,
        "36": shape_36,
        "37": shape_37,
        "38a": shape_38,
        "38b": shape_38,
        "39a": shape_39,
        "39b": shape_39,
        "40": shape_40,
        "41": shape_41,
        "42": shape_42,
        "43": shape_43,
        "44a": shape_44,
        "44b": shape_44,
        "45": shape_45,
        "46a": shape_46,
        "46b": shape_46,
        "47": shape_47,
        "48": shape_48,
    }
}

function draw() {
    background(bg)
    ambientLight(bg)    
    cam_pos_x = cam_dist * cos(cam_angle_y) * cos(cam_angle_x)
    cam_pos_y = sin(cam_angle_y) * cam_dist
    cam_pos_z = cam_dist * sin(cam_angle_x) * cos(cam_angle_y)
    camera(cam_pos_x, cam_pos_y, cam_pos_z, 0, 0, 0, 0, cam_up_y, 0)
    animation = animation_list[anim]

    function starting_point_from_up() {
        piece_pos_x = 0
        piece_pos_y = -hcs
    }
    function starting_point_from_down() {
        piece_pos_x = 0
        piece_pos_y = hcs
    }
    function starting_point_from_left() {
        piece_pos_x = -hcs
        piece_pos_y = 0
    }
    function starting_point_from_right() {
        piece_pos_x = hcs
        piece_pos_y = 0
    }
    let start_point = {
        'up': starting_point_from_up,
        'down': starting_point_from_down,
        'left': starting_point_from_left,
        'right': starting_point_from_right
    }
    
    start_angle = {
        'model_1': null,
        'model_2': null,
        'model_3': null,
        'model_4': {
            'down': 180,
            'right': 270
        },
        'model_5': {
            'up': 0,
            'down': 180,
            'left': 90,
            'right': 270
        },
        'model_6': null,
        'model_7': null,
        'model_8': {
            'down': 180,
            'right': 270
        }
    }

    function next_shape() {
        new_shape = path_enable[last_shape]
        last_shape = current_shape
        current_shape = new_shape[0]
        shape_id = shapes[current_shape].get_id()
        shape_model_name = shapes[current_shape].get_model_name()
        come_from = new_shape[1]
        start_point[come_from]()
        if (start_angle[shape_model_name] != null) {
            agl_rot_piece = start_angle[shape_model_name][come_from]
        }
        is_input = true
    }

    function come_from_up(val = 0) {
        piece_pos_y += piece_anim_speed
        if (piece_pos_y >= val) {
            piece_pos_y = val
            is_input = false
            piece_anim = false
        }
    }
    function come_from_down(val = 0) {
        piece_pos_y -= piece_anim_speed
        if (piece_pos_y <= val) {
            piece_pos_y = val
            is_input = false
            piece_anim = false
        }
    }
    function come_from_left(val = 0) {
        piece_pos_x += piece_anim_speed
        if (piece_pos_x >= val) {
            piece_pos_x = val
            is_input = false
            piece_anim = false
        }
    }
    function come_from_right(val = 0) {
        piece_pos_x -= piece_anim_speed
        if (piece_pos_x <= val) {
            piece_pos_x = val
            is_input = false
            piece_anim = false
        }
    }
    function exit_to_up() {
        piece_pos_y -= piece_anim_speed
        if (piece_pos_y <= -hcs) {
            next_shape()
        }
    }
    function exit_to_down() {
        piece_pos_y += piece_anim_speed
        if (piece_pos_y >= hcs) {
            next_shape()
        }
    }
    function exit_to_left() {
        piece_pos_x -= piece_anim_speed
        if (piece_pos_x <= -hcs) {
            next_shape()
        }
    }
    function exit_to_right() {
        piece_pos_x += piece_anim_speed
        if (piece_pos_x >= hcs) {
            next_shape()
        }
    }
    function come_turning_from_up() {
        agl_rot_piece += agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) - hcs
        piece_pos_y = hcs * sin(agl_rot_piece) - hcs
        if (agl_rot_piece >= 45) {
            agl_rot_piece = 45
            is_input = false
            piece_anim = false
        }
    }
    function come_turning_from_down() {
        agl_rot_piece += agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) + hcs
        piece_pos_y = hcs * sin(agl_rot_piece) + hcs
        if (agl_rot_piece >= 225) {
            agl_rot_piece = 225
            is_input = false
            piece_anim = false
        }
    }
    function come_turning_from_left() {
        agl_rot_piece -= agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) - hcs
        piece_pos_y = hcs * sin(agl_rot_piece) - hcs
        if (agl_rot_piece <= 45) {
            agl_rot_piece = 45
            is_input = false
            piece_anim = false
        }
    }
    function come_turning_from_right() {
        agl_rot_piece -= agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) + hcs
        piece_pos_y = hcs * sin(agl_rot_piece) + hcs
        if (agl_rot_piece <= 225) {
            agl_rot_piece = 225
            is_input = false
            piece_anim = false
        }
    }
    function exit_turning_to_up() {
        agl_rot_piece -= agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) - hcs
        piece_pos_y = hcs * sin(agl_rot_piece) - hcs
        if (agl_rot_piece <= 0) {
            next_shape()
        }
    }
    function exit_turning_to_down() {
        agl_rot_piece -= agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) + hcs
        piece_pos_y = hcs * sin(agl_rot_piece) + hcs
        if (agl_rot_piece <= 180) {
            next_shape()
        }
    }
    function exit_turning_to_left() {
        agl_rot_piece += agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) - hcs
        piece_pos_y = hcs * sin(agl_rot_piece) - hcs
        if (agl_rot_piece >= 90) {
            next_shape()
        }
    }
    function exit_turning_to_right() {
        agl_rot_piece += agl_rot_piece_speed
        piece_pos_x = hcs * cos(agl_rot_piece) + hcs
        piece_pos_y = hcs * sin(agl_rot_piece) + hcs
        if (agl_rot_piece >= 270) {
            next_shape()
        }
    }
    //////////////  animation path model_1  ///////////////
    function _model_1() {
        if (is_input) {
            // enter into a new shape
            come_from_down()
        } else {
            // exit from the current shape
            exit_to_down()
        }
    }
    //////////////  animation path model_2  ///////////////
    function _model_2() {
        if (come_from == 'up'){
            if (is_input) {
                // enter into a new shape
                come_from_up()
            } else {
                // exit from the current shape
                exit_to_down()
            }
        }
        else if (come_from == 'down'){
            if (is_input) {
                // enter into a new shape
                come_from_down()
            } else {
                // exit from the current shape
                exit_to_up()
            }
        }
        else if (come_from == 'left'){
            if (is_input) {
                // enter into a new shape
                come_from_left()
            } else {
                // exit from the current shape
                exit_to_right()
            }
        }
        else if (come_from == 'right'){
            if (is_input) {
                // enter into a new shape
                come_from_right()
            } else {
                // exit from the current shape
                exit_to_left()
            }
        }
    }
    //////////////  animation path model_3  ///////////////
    function _model_3() {
        if (come_from == 'up'){
            if (is_input) {
                // enter into a new shape
                come_from_up()
            } else {
                // exit from the current shape
                exit_to_down()
            }
        }
        else if (come_from == 'down'){
            if (is_input) {
                // enter into a new shape
                come_from_down()
            } else {
                // exit from the current shape
                exit_to_up()
            }
        }
    }
    //////////////  animation path model_4  ///////////////
    function _model_4() {
        if (come_from == 'down'){
            if (is_input) {
                // enter into a new shape
                come_turning_from_down()
            } else {
                // exit from the current shape
                exit_turning_to_right()
            }
        }
        else if (come_from == 'right'){
            if (is_input) {
                // enter into a new shape
                come_turning_from_right()
            } else {
                // exit from the current shape
                exit_turning_to_down()
            }
        }
    }
    //////////////  animation path model_5  ///////////////
    function _model_5() {
        if (come_from == 'up') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_up()
            } else {
                // exit from the current shape
                exit_turning_to_left()
            }
        }
        else if (come_from == 'down') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_down()
            } else {
                // exit from the current shape
                exit_turning_to_right()
            }
        }
        else if (come_from == 'left') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_left()
            } else {
                // exit from the current shape
                exit_turning_to_up()
            }
        }
        else if (come_from == 'right') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_right()
            } else {
                // exit from the current shape
                exit_turning_to_down()
            }
        }
    }
    //////////////  animation path model_6  ///////////////
    function _model_6() {
        if (come_from == 'down') {
            if (is_input) {
                // enter into a new shape
                come_from_down(10)
            } else {
                // exit from the current shape
                exit_to_down()
            }
        }
        else if (come_from == 'right') {
            if (is_input) {
                // enter into a new shape
                come_from_right(10)
            } else {
                // exit from the current shape
                exit_to_right()
            }
        }
    }
    //////////////  animation path model_7  ///////////////
    function _model_7() {
        if (come_from == 'up') {
            if (is_input) {
                // enter into a new shape
                come_from_up(-10)
            } else {
                // exit from the current shape
                exit_to_up()
            }
        }
        else if (come_from == 'left') {
            if (is_input) {
                // enter into a new shape
                come_from_left()
            } else {
                // exit from the current shape
                exit_to_right()
            }
        }
        else if (come_from == 'right') {
            if (is_input) {
                // enter into a new shape
                come_from_right()
            } else {
                // exit from the current shape
                exit_to_left()
            }
        }
    }
    //////////////  animation path model_8  ///////////////
    function _model_8() {
        if (come_from == 'up') {
            if (is_input) {
                // enter into a new shape
                come_from_up(-10)
            } else {
                // exit from the current shape
                exit_to_up()
            }
        }
        else if (come_from == 'down') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_down()
            } else {
                // exit from the current shape
                exit_turning_to_right()
            }
        }
        else if (come_from == 'right') {
            if (is_input) {
                // enter into a new shape
                come_turning_from_right()
            } else {
                // exit from the current shape
                exit_turning_to_down()
            }
        }
    }
    let model_is = {
        "model_1": _model_1,
        "model_2": _model_2,
        "model_3": _model_3,
        "model_4": _model_4,
        "model_5": _model_5,
        "model_6": _model_6,
        "model_7": _model_7,
        "model_8": _model_8
    }
    // piece animation
    if (piece_anim == true) {
        model_is[model_name]()
    }

    // zoom animation
    if (zoom == true) {
        cam_dist -= 10
        if (cam_dist <= 150) {
            cam_dist = 150
        }
    } else {
        cam_dist += 10
        if (cam_dist >= 400) {
            cam_dist = 400
        }
    }

    // camera animation around cube
    if (keyIsDown(RIGHT_ARROW)) {
        cam_angle_x -= cam_rot_speed
    }
    if (keyIsDown(LEFT_ARROW)) {
        cam_angle_x += cam_rot_speed
    }
    if (keyIsDown(UP_ARROW)) {
        cam_angle_y -= cam_rot_speed
    }
    if (keyIsDown(DOWN_ARROW)) {
        cam_angle_y += cam_rot_speed
    }
    if (cam_angle_y > 90 || cam_angle_y < -90) {
        cam_up_y = -1
    } else {
        cam_up_y = 1
    }
    if (cam_angle_y < -179) {
        cam_angle_y = 180
    } else if (cam_angle_y > 180) {
        cam_angle_y = -179
    }

    // let animation play until 180 degree is reached
    function angle_rotation() {
        angle += rot_speed
        if (angle > 180) {
            angle = 180
            if (piece_anim == false) {
                manip = true
            }
        }
    }
    
    // animation transition between cube configurations
    if (animation == 0) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs / (180 / rot_speed)
            if (pos_y > 0) {
                pos_y = 0
                configuration = 0
            }
            cubes[0].set_trans(-cs, 0, -hcs)
            cubes[0].set_pos(hcs, -hcs, 0)
            cubes[0].set_rot(0, 0, -180 + angle)

            cubes[1].set_rot(0, 0, 180 - angle)
            cubes[3].set_rot(0, 0, 180 - angle)
            cubes[5].set_rot(0, 0, -180 + angle)
            cubes[7].set_rot(0, 0, -180 + angle)
        } else {
            angle_rotation()
            pos_y -= hcs / (180 / rot_speed)
            if (pos_y < -hcs) {
                pos_y = -hcs
                configuration = 1
            }
            cubes[0].set_rot(0, 0, -angle)

            cubes[1].set_rot(0, 0, angle)
            cubes[3].set_rot(0, 0, angle)
            cubes[5].set_rot(0, 0, -angle)
            cubes[7].set_rot(0, 0, -angle)
        }
    }
    if (animation == 1) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs / (90 / rot_speed)
            if (pos_y > -hcs) {
                pos_y = -hcs
                configuration = 1
            }
            cubes[0].set_trans(-cs, cs, 0)
            cubes[0].set_pos(hcs, hcs, -hcs)
            cubes[0].set_rot(90 - angle / 2, 0, -180)

            cubes[4].set_rot(180 - angle, 0, 0)
        } else {
            angle_rotation()
            pos_y -= hcs / (90 / rot_speed)
            if (pos_y < -hcs * 3) {
                pos_y = -hcs * 3
                configuration = 2
            }
            cubes[0].set_trans(-cs, cs, 0)
            cubes[0].set_pos(hcs, hcs, -hcs)
            cubes[0].set_rot(angle / 2, 0, -180)

            cubes[4].set_rot(angle, 0, 0)
        }
    }
    if (animation == 2) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs * 3 / (180 / rot_speed)
            if (pos_y > -hcs * 3) {
                pos_y = -hcs * 3
                configuration = 2
            }
            cubes[0].set_trans(0, cs * 2, 0)
            cubes[0].set_pos(hcs + cs, hcs, hcs)
            cubes[0].set_rot(90, -90 + angle / 2, -180)

            cubes[2].set_rot(0, 180 - angle, 0)
            cubes[6].set_rot(0, 180 - angle, 0)
        } else {
            angle_rotation()
            pos_y -= hcs * 3 / (180 / rot_speed)
            if (pos_y < -cs * 3) {
                pos_y = -cs * 3
                configuration = 3
            }
            cubes[0].set_trans(0, cs * 2, 0)
            cubes[0].set_pos(hcs + cs, hcs, hcs)
            cubes[0].set_rot(90, -angle / 2, -180)

            cubes[2].set_rot(0, angle, 0)
            cubes[6].set_rot(0, angle, 0)
        }
    }
    if (animation == 3) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs / (180 / rot_speed)
            if (pos_y > -cs * 3) {
                pos_y = -cs * 3
                configuration = 3
            }
            cubes[1].set_rot(0, 0, angle)
            cubes[3].set_rot(0, 0, angle)
            cubes[5].set_rot(0, 0, 360 - angle)
            cubes[7].set_rot(0, 0, 360 - angle)
        } else {
            angle_rotation()
            pos_y -= hcs / (180 / rot_speed)
            if (pos_y < -hcs * 7) {
                pos_y = -hcs * 7
                configuration = 4
            }
            cubes[1].set_rot(0, 0, -angle + 180)
            cubes[3].set_rot(0, 0, -angle + 180)
            cubes[5].set_rot(0, 0, angle + 180)
            cubes[7].set_rot(0, 0, angle + 180)
        }
    }
    if (animation == 4) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs / (90 / rot_speed)
            if (pos_y > -hcs * 7) {
                pos_y = -hcs * 7
                configuration = 4
            }
            cubes[0].set_trans(0, cs * 4, 0)
            cubes[0].set_pos(-hcs, hcs, hcs)
            cubes[0].set_rot(90, -180 + angle / 2, -180)

            cubes[2].set_rot(0, angle, 0)
            cubes[6].set_rot(0, angle, 0)
        } else {
            angle_rotation()
            pos_y -= hcs / (90 / rot_speed)
            if (pos_y < -hcs * 9) {
                pos_y = -hcs * 9
                configuration = 5
            }
            cubes[0].set_trans(0, cs * 4, 0)
            cubes[0].set_pos(-hcs, hcs, hcs)
            cubes[0].set_rot(90, -angle / 2 - 90, -180)

            cubes[2].set_rot(0, -angle + 180, 0)
            cubes[6].set_rot(0, -angle + 180, 0)
        }
    }
    if (animation == 5) {
        if (reverse) {
            angle_rotation()
            pos_y += hcs * 3 / (180 / rot_speed)
            if (pos_y > -hcs * 9) {
                pos_y = -hcs * 9
                configuration = 5
            }
            cubes[0].set_trans(0, cs * 5, 0)
            cubes[0].set_pos(-hcs, hcs, -hcs)
            cubes[0].set_rot(180 - angle / 2, -180, -180)

            cubes[4].set_rot(angle, 0, 0)
        } else {
            angle_rotation()
            pos_y -= hcs * 3 / (180 / rot_speed)
            if (pos_y < -cs * 6) {
                pos_y = -cs * 6
                configuration = 0
            }
            cubes[0].set_trans(0, cs * 5, 0)
            cubes[0].set_pos(-hcs, hcs, -hcs)
            cubes[0].set_rot(angle / 2 + 90, -180, -180)
    
            cubes[4].set_rot(-angle + 180, 0, 0)
        }
    }

    // show planets
    for (let p of planets) {
        p.show()
    }
    
    // show cubes
    translate(0, pos_y, 0)
    for (let c of cubes) {
        c.show()
    }
}
