function empty_shape() {
    return undefined
}

function flag() {
    noFill()
    stroke(0)
    beginShape()
    vertex(0, 0, 0)
    vertex(0, 0, hcs)
    vertex(0, -hcs / 2, hcs * 3 / 4)
    vertex(0, 0, hcs / 2)
    endShape()
}

function flag_deco(fn) {
    return function() {
        flag()
        fn()
    }
}
const myflag = flag_deco(model_1)

function model_1() { // dead-end
    noFill()
    stroke(0)
    beginShape()
    vertex(-hcs / 5, hcs, 0)
    vertex(-hcs / 5, -hcs / 5, 0)
    vertex(hcs / 5, -hcs / 5, 0)
    vertex(hcs / 5, hcs, 0)
    endShape()
}
function model_2() { // cross road
    noFill()
    stroke(0)
    beginShape(LINES)
    vertex(-hcs / 5, -hcs, 0)
    vertex(-hcs / 5, hcs, 0)
    vertex(hcs / 5, hcs, 0)
    vertex(hcs / 5, -hcs, 0)
    vertex(-hcs, -hcs / 5, 0)
    vertex(-hcs / 5, -hcs / 5, 0)
    vertex(hcs / 5, -hcs / 5, 0)
    vertex(hcs, -hcs / 5, 0)
    vertex(-hcs, hcs / 5, 0)
    vertex(-hcs / 5, hcs / 5, 0)
    vertex(hcs / 5, hcs / 5, 0)
    vertex(hcs, hcs / 5, 0)
    endShape()
}
function model_3() { // straight road
    noFill()
    stroke(0)
    beginShape(LINES)
    vertex(-hcs / 5, -hcs, 0)
    vertex(-hcs / 5, hcs, 0)
    vertex(hcs / 5, hcs, 0)
    vertex(hcs / 5, -hcs, 0)
    endShape()
}
function model_4() { // turning point
    noFill()
    stroke(0)
    beginShape(LINES)
    vertex(-hcs / 5, hcs, 0)
    quadraticVertex(-hcs / 8, -hcs / 8, 0, hcs, -hcs / 5, 0)
    vertex(hcs / 5, hcs, 0)
    quadraticVertex(hcs / 4, hcs / 4, 0, hcs, hcs / 5, 0)
    endShape()
}
function model_5() { // double turning point
    noFill()
    stroke(0)
    beginShape(LINES)
    vertex(-hcs, hcs / 5, 0)
    quadraticVertex(hcs / 8, hcs / 8, 0, hcs / 5, -hcs, 0)
    vertex(-hcs, -hcs / 5, 0)
    quadraticVertex(-hcs / 4, -hcs / 4, 0, -hcs / 5, -hcs, 0)
    vertex(-hcs / 5, hcs, 0)
    quadraticVertex(-hcs / 8, -hcs / 8, 0, hcs, -hcs / 5, 0)
    vertex(hcs / 5, hcs, 0)
    quadraticVertex(hcs / 4, hcs / 4, 0, hcs, hcs / 5, 0)
    endShape()
}
function model_6() { // double dead-end
    noFill()
    stroke(0)
    beginShape()
    vertex(-hcs / 5, hcs, 0)
    vertex(-hcs / 5, hcs - hcs / 2, 0)
    vertex(hcs / 5, hcs - hcs / 2, 0)
    vertex(hcs / 5, hcs, 0)
    endShape()
    beginShape()
    vertex(hcs, -hcs / 5, 0)
    vertex(hcs - hcs / 2, -hcs / 5, 0)
    vertex(hcs - hcs / 2, hcs / 5, 0)
    vertex(hcs, hcs / 5, 0)
    endShape()
}
function model_7() { // straight line & dead-end
    noFill()
    stroke(0)
    beginShape(LINES)
    vertex(-hcs, hcs / 5, 0)
    vertex(hcs, hcs / 5, 0)
    vertex(hcs, -hcs / 5, 0)
    vertex(-hcs, -hcs / 5, 0)
    endShape()
    beginShape()
    vertex(-hcs / 5, -hcs, 0)
    vertex(-hcs / 5, -hcs + hcs / 2, 0)
    vertex(hcs / 5, -hcs + hcs /  2, 0)
    vertex(hcs / 5, -hcs, 0)
    endShape()
}
function model_8() { // turning point & dead-end
    noFill()
    stroke(0)
    beginShape()
    vertex(-hcs / 5, -hcs, 0)
    vertex(-hcs / 5, -hcs + hcs / 2, 0)
    vertex(hcs / 5, -hcs + hcs / 2, 0)
    vertex(hcs / 5, -hcs, 0)
    endShape()
    beginShape(LINES)
    vertex(-hcs / 5, hcs, 0)
    quadraticVertex(-hcs / 8, -hcs / 8, 0, hcs, -hcs / 5, 0)
    vertex(hcs / 5, hcs, 0)
    quadraticVertex(hcs / 4, hcs / 4, 0, hcs, hcs / 5, 0)
    endShape()
}