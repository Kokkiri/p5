function keyPressed() {
    if (keyCode === 13) { // 13 == enter
        let fs = fullscreen();
        fullscreen(!fs);
    }
    if (keyCode === 85) { // 85 == u
        zoom = !zoom
    }
    if (manip == true) {
        if (keyCode === 65) { // 65 == a
            manip = false
            if (launching) {
                anim = -1
                reverse = false
                launching = false
            }
            angle = 0
            if (reverse == false) {
                anim += 1
            }
            anim %= animation_list.length
            if (anim == 0) {
                pos_y = 0
                cubes[0].set_trans(-cs, 0, -hcs)
                cubes[0].set_pos(hcs, -hcs, 0)
                cubes[0].set_rot(0, 0, 0)
            }
            reverse = false
        }
        if (keyCode === 69) { // 69 == e
            manip = false
            if (launching) {
                anim = 6
                reverse = true
                launching = false
            }
            angle = 0
            if (reverse == true) {
                anim -= 1
            }
            if (anim < 0) {
                anim = animation_list.length - 1
            }
            if (anim == 5) {
                pos_y = -cs * 6
                cubes[0].set_trans(0, cs * 5, 0)
                cubes[0].set_pos(-hcs, hcs, -hcs)
                cubes[0].set_rot(180, -180, -180)
            }
            reverse = true
        }
        path_enable = path[current_shape][String(configuration)]
        if (keyCode === 73) { // 73 == i
            if (path_enable != null) {
                if (path_enable[last_shape] != null) {
                    manip = false
                    piece_anim = true
                }
            }
        }
    }
}

function mouseWheel() {
    if (cam_dist == 150 || cam_dist == 400) {
        zoom = !zoom
    }
}

function mouseDragged() {
    cam_angle_x += movedX
    cam_angle_y -= movedY
    redraw()
}
