class Cube {
    constructor(x, y, z) {
        this.pos = createVector(x, y, z)
        this.trans = createVector(0, 0, 0)
        this.rot = createVector(0, 0, 0)
        this.r = 0
        this.g = 0
        this.b = 0
        this.list_shape = {
            'shape_1': empty_shape,
            'shape_2': empty_shape,
            'shape_3': empty_shape,
            'shape_4': empty_shape,
            'shape_5': empty_shape,
            'shape_6': empty_shape
        }
    }
    set_pos(x, y, z) {
        this.pos = createVector(x, y, z)
    }
    set_rot(x, y, z) {
        this.rot = createVector(x, y, z)
    }
    set_color(r, g, b) {
        this.r = r
        this.g = g
        this.b = b
    }
    set_trans(x, y, z) {
        this.trans = createVector(x, y, z)
    }
    set_shape(shape_1, shape_2, shape_3, shape_4, shape_5, shape_6) {
        this.list_shape['shape_1'] = shape_1 // right
        this.list_shape['shape_2'] = shape_2 // front
        this.list_shape['shape_3'] = shape_3 // left
        this.list_shape['shape_4'] = shape_4 // back
        this.list_shape['shape_5'] = shape_5 // up
        this.list_shape['shape_6'] = shape_6 // down
    }
    show() {
        translate(this.trans.x, this.trans.y, this.trans.z) // translate axis rotation
        rotateX(this.rot.x)
        rotateY(this.rot.y)
        rotateZ(this.rot.z)
        this.list_shape['shape_1'].show(this.pos.x + hcs, this.pos.y, this.pos.z)
        this.list_shape['shape_2'].show(this.pos.x, this.pos.y, this.pos.z + hcs)
        this.list_shape['shape_3'].show(this.pos.x - hcs, this.pos.y, this.pos.z)
        this.list_shape['shape_4'].show(this.pos.x, this.pos.y, this.pos.z - hcs)
        this.list_shape['shape_5'].show(this.pos.x, this.pos.y - hcs, this.pos.z)
        this.list_shape['shape_6'].show(this.pos.x, this.pos.y + hcs, this.pos.z)
        translate(this.pos.x, this.pos.y, this.pos.z) // translate box around axis rotation
    }
}

class Shape {
    constructor(id, model, agl_x, agl_y, agl_z, color) {
        this.id = id
        this.model = model
        this.agl_x = agl_x
        this.agl_y = agl_y
        this.agl_z = agl_z
        this.color = color
    }
    get_id() {
        return this.id
    }
    get_model_name() {
        return this.model.name
    }
    show(x, y, z) {
        push()
        fill(this.color)
        noStroke()
        translate(x, y, z)
        rotateX(this.agl_x)
        rotateY(this.agl_y)
        rotateZ(this.agl_z)
        plane(cs)
        this.model()
        if (this.id == 1 || this.id == 48) {
            flag()
        }
        if (shape_id == this.id) {
            model_name = this.model.name
            piece.set_pos(piece_pos_x, piece_pos_y)
            piece.show()
        }
        pop()
    }
}

class Piece {
    constructor() {
        this.x = 0
        this.y = 0
        this.z = 0
        this.color = red
    }
    set_pos(x, y) {
        this.x = x
        this.y = y
    }
    set_color(color) {
        this.color = color
    }
    show() {
        push()
        fill(this.color)
        noStroke()
        translate(this.x, this.y, this.z)
        sphere(2.5)
        pop()
    }
}

class Planet {
    constructor() {
        this.max_dist = 1500
        this.min_dist = 1000
        this.min_size = 10
        this.max_size = 30
        this.min_color = 100
        this.max_color = 200
        this.dist = Math.random() * (this.max_dist - this.min_dist + 1) + this.min_dist;
        this.size = Math.random() * (this.max_size - this.min_size + 1) + this.min_size;
        this.r = Math.random() * (this.max_color - this.min_color + 1) + this.min_color;
        this.g = Math.random() * (this.max_color - this.min_color + 1) + this.min_color;
        this.b = Math.random() * (this.max_color - this.min_color + 1) + this.min_color;
        this.agl_x = random() * 360
        this.agl_y = random() * 360
        this.pos_x = this.dist * cos(this.agl_y) * cos(this.agl_x)
        this.pos_y = sin(this.agl_y) * this.dist
        this.pos_z = this.dist * sin(this.agl_x) * cos(this.agl_y)
    }
    show() {
        push()
        translate(this.pos_x, this.pos_y, this.pos_z)
        fill(this.r, this.g, this.b)
        noStroke()
        sphere(this.size)
        pop()
    }
}