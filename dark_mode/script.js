const dark_mode = document.getElementById('dark-mode')

dark_mode.addEventListener('change', () => {
    document.body.classList.toggle('dark')
})
