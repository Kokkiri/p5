class Filtre {
    constructor(img) {
        this.img = img
        this.current_pos = -600
    }
    get_pos() {
        return this.current_pos
    }
    set_pos(pos) {
        this.current_pos = pos
    }
    show() {
        push()
        translate(this.current_pos, 0 ,0, 0)
        push()
        noStroke()
        noFill()
        rect(0, 0, 600, 600)
        pop()
        image(this.img, width / 2, height / 2, this.img.width * ratio, this.img.height * ratio)
        pop()
    }
}