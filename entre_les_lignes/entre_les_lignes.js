let total_filtres = 5
let filtre = 0
let ratio = 1.5
let prev_pos = -600
let next_pos = 0
let anim = false
let speed = 15
let launch = false
let reverse = false

function preload() {
    article = loadImage('img/article_re/article.png')
    fil_4 = loadImage('img/article_re/filtre_1.png')
    fil_3 = loadImage('img/article_re/filtre_2.png')
    fil_2 = loadImage('img/article_re/filtre_3.png')
    fil_1 = loadImage('img/article_re/filtre_4.png')
    // article = loadImage('img/article.png')
    // fil_1 = loadImage('img/filtre_1.png')
    // fil_2 = loadImage('img/filtre_2.png')
    // fil_3 = loadImage('img/filtre_3.png')
    // fil_4 = loadImage('img/filtre_4.png')
}

function setup() {
    imageMode(CENTER)
    createCanvas(600, 600)

    article.resize(400, 250)
    fil_1.resize(400, 250)
    fil_2.resize(400, 250)
    fil_3.resize(400, 250)
    fil_4.resize(400, 250)

    filtre_1 = new Filtre(fil_1)
    filtre_2 = new Filtre(fil_2)
    filtre_3 = new Filtre(fil_3)
    filtre_4 = new Filtre(fil_4)
}

function draw() {
    background(color(230, 230, 230))
    image(article, width/2, height/2, article.width * ratio, article.height * ratio)
    
    if (anim == true) {
        if (reverse == false) {
            prev_pos += speed
            next_pos += speed
            if (prev_pos >= 0) {
                prev_pos = 0
                next_pos = 600
                anim = false
            }
        } else {
            prev_pos -= speed
            next_pos -= speed
            if (next_pos <= 0) {
                next_pos = 0
                prev_pos = -600
                anim = false
            }
        }
    }

    if (filtre == 0) {
        if (reverse) {
            filtre_1.set_pos(prev_pos)
            filtre_1.show()
        } else {
            if (launch) {
                filtre_4.set_pos(next_pos)
                filtre_4.show()
            }
        }
    }

    if (filtre == 1) {
        if (reverse) {
            filtre_2.set_pos(prev_pos)
            filtre_1.set_pos(next_pos)
            filtre_2.show()
            filtre_1.show()
        } else {
            filtre_1.set_pos(prev_pos)
            filtre_1.show()
        }
    }
    if (filtre == 2) {
        if (reverse) {
            filtre_3.set_pos(prev_pos)
            filtre_2.set_pos(next_pos)
            filtre_3.show()
            filtre_2.show()
        } else {
            filtre_1.set_pos(next_pos)
            filtre_2.set_pos(prev_pos)
            filtre_1.show()
            filtre_2.show()
        }
    }
    if (filtre == 3) {
        if (reverse) {
            filtre_4.set_pos(prev_pos)
            filtre_3.set_pos(next_pos)
            filtre_4.show()
            filtre_3.show()
        } else {
            filtre_2.set_pos(next_pos)
            filtre_3.set_pos(prev_pos)
            filtre_2.show()
            filtre_3.show()
        }
    }
    if (filtre == 4) {
        if (reverse) {
            filtre_4.set_pos(next_pos)
            filtre_4.show()
        } else {
            filtre_3.set_pos(next_pos)
            filtre_4.set_pos(prev_pos)
            filtre_3.show()
            filtre_4.show()
        }
    }
}

function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        if (anim == false) {
            filtre -= 1
            if (filtre == -1) {
                filtre = total_filtres - 1
            }
            prev_pos = 0
            next_pos = 600
            launch = true
            anim = true
            reverse = true
        }
    }
    if (keyCode === RIGHT_ARROW) {
        if (anim == false) {
            filtre += 1
            filtre %= total_filtres
            prev_pos = -600
            next_pos = 0
            launch = true
            anim = true
            reverse = false
        }
    }
}