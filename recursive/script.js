// FUNCTION RECURSIVE BASIC
function countdown(num) {
    if (num < 0) {
        return
    }
    console.log(num)
    setTimeout(
        countdown, 1000, num-1
    )
}
// countdown(10)

// FUNCTION RECURSIVE CLASSIC
// sum(n) = n + sum(n - 1)
const sum = (n) => {
    if (n <= 1) {
        return n
    }
    return n + sum(n - 1)
}
// console.log('sum(10)', sum(5))

let count = 0

// LISTE ALL SEQUENCES OF 5 * 2 POSSIBILITYS
function show_list() {
    for (let i = 0; i < 2; i++) {
        for (let j = 0; j < 2; j++) {
            for (let k = 0; k < 2; k++) {
                for (let l = 0; l < 2; l++) {
                    for (let m = 0; m < 2; m++) {
                        console.log(i, j, k, l, m)
                        count += 1
                    }
                }
            }
        }
    }
    console.log('count', count)
}
// show_list()

let sequence = []
// IDEM WITH RECURSIVITY
function show_list_recursively() {
    // if (sequence[0].lenght > 5) {
    //     return sequence
    // }
    for (let i = 0; i < 5; i++) {
        for (let i = 0; i < 2; i++) {
            sequence.push(i)
        }
    }
    console.log(sequence)
    // show_list_recursively()
}

show_list_recursively()
