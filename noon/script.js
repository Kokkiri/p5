
function create_noon() {
    let ratio = Math.random() * 5
    let noon = document.createElement('div')
    noon.style.width = 1 * ratio + 'px'
    noon.style.height = 1 * ratio + 'px'
    noon.style.animationDuration = 10 / ratio + 's'
    noon.className = 'noon'
    noon.style.left = Math.random() * (window.innerWidth - 20) + 10 + 'px'
    document.body.appendChild(noon)
    setTimeout(() => {
        noon.remove()
    }, 5000)
}

setInterval(() => {
    create_noon()
}, 10)

